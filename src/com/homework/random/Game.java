package com.homework.random;

import java.util.Random;
import java.util.Scanner;

public class Game {
    public static void main(String[] args) {

        Random rnd = new Random();
        int rndNumber = rnd.nextInt(100)+1;
        System.out.println("Let the game begin!");
        System.out.println("What is your name?");
        Scanner keyboard = new Scanner(System.in);
        String userName = keyboard.nextLine();


        while(true){
            System.out.println("Please enter a number between 0 and 100:");
            int userNumber = keyboard.nextInt();
            if (userNumber <0 || userNumber >100) {
                System.out.println("Pleae enter number correct");
                continue;
            }
            if (userNumber < rndNumber){
                System.out.println("Your number is too small. Please, try again.");
            } else if (userNumber > rndNumber) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.println("Congratulations" + userName);
                break;
            }
        }
        keyboard.close();
    }
}
